package com.example.myapp;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
    Button btnOpnBr, btnOpnPhn, btnBrwV, btnBrwL,btnExpCon;
    String msg = "Android : ";
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Log.d(msg, "The onCreate() event");

        /**BUTTONS*/
        btnOpnBr = (Button) findViewById(R.id.btnBrowser);
        btnOpnBr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com.ua"));
                startActivity(i);
            }
        });
        btnOpnPhn = (Button) findViewById(R.id.btnPhone);
        btnOpnPhn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:0634817062"));
                startActivity(i);
            }
        });

        btnBrwV = (Button) findViewById(R.id.buttonStartBrowsingwVIEW);
        btnBrwV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.example.com"));
                startActivity(i);
            }
        });

        btnBrwL=(Button)findViewById(R.id.buttonStartBrowsingwLAUNCH);
        btnBrwL.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent("com.example.myapp.LAUNCH",Uri.parse("http://www.example.com"));
                startActivity(i);
            }
        });

        btnExpCon=(Button)findViewById(R.id.buttonExceptional); /**FATAL ERROR no valid filter for HTTPS*/
        btnExpCon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent("com.example.myapp.LAUNCH",Uri.parse("https://www.example.com"));
                startActivity(i);
            }
        });
    }

    /** Inflate the menu; this adds items to the action bar if it is present.*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /** Handle action bar item clicks here. The action bar will
     automatically handle clicks on the Home/Up button, so long
     as you specify a parent activity in AndroidManifest.xml.*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Log.d(msg, "In onOptionsItemSelected(): ");

        Cursor cursor = getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_main) {
            Log.d(msg, "In first if()");
            if (cursor.moveToFirst()) {
                Log.d(msg, "In second if()");
                do {
                    String msgData = "";
                    for (int index = 0; index < cursor.getColumnCount(); index++)
                    {
                        msgData += " " + cursor.getColumnName(index) + ":" + cursor.getString(index);
                    }
                    Log.d(msg, "In do{}");
                    Toast.makeText(this, msgData, Toast.LENGTH_LONG);
                } while (cursor.moveToNext());
            }
            Log.d(msg, "after second if()");
            return true;
        }
        Log.d(msg, "after first if()");
        return super.onOptionsItemSelected(item);
    }

    /** Called when the activity is about to become visible. */
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(msg, "The onStart() event");
    }

    /** Called when the activity has become visible. */
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(msg, "The onResume() event");
    }

    /** Called when another activity is taking focus. */
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(msg, "The onPause() event");
    }

    /** Called when the activity is no longer visible. */
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(msg, "The onStop() event");
    }

    /** Called just before the activity is destroyed. */
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(msg, "The onDestroy() event");
    }

    // Method to start the service
    public void startService(View view) {
        startService(new Intent(getBaseContext(), MyService.class));
    }

    // Method to stop the service
    public void stopService(View view) {
        stopService(new Intent(getBaseContext(), MyService.class));
    }

    // broadcast a custom intent.
    public void broadcastIntent(View view){
        Intent intent = new Intent();
        intent.setAction("com.tutorialspoint.CUSTOM_INTENT");
        sendBroadcast(intent);
    }

    // Add a new student record
    public void onClickAddName (View view) {
        ContentValues values = new ContentValues();

        values.put(StudentsProvider.NAME, ((EditText) findViewById(R.id.editTextName)).getText().toString());
        values.put(StudentsProvider.GRADE, ((EditText) findViewById(R.id.editTextGrade)).getText().toString());

        Uri uri = getContentResolver().insert(StudentsProvider.CONTENT_URI, values);

        Toast.makeText(getBaseContext(), uri.toString(),Toast.LENGTH_LONG).show();
    }

    // Retrieve students records
    public void onClickRetrieveStudents(View view) {
        String URL = "content://com.example.provider.College/students";

        Uri students = Uri.parse(URL);
        Cursor c = managedQuery(students, null, null,null, "name");

        if (c.moveToFirst()) {
            do {
                Toast.makeText(this,
                    c.getString(c.getColumnIndex(StudentsProvider._ID)) +
                            ", " + c.getString(c.getColumnIndex(StudentsProvider.NAME)) +
                            ", " + c.getString(c.getColumnIndex(StudentsProvider.GRADE)),
                    Toast.LENGTH_SHORT).show();
            } while (c.moveToNext());
        }
    }

}
