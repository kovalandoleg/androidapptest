package com.example.myapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by nook on 12.01.16.
 */
public class splash extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        Thread logoTimer = new Thread(){
            @Override
            public void run() {
                try{
                    sleep(5000);
                    Intent mainIntent = new Intent("MainScreenActivity");
                    startActivity(mainIntent);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    finish();
                }
            }
        };
        logoTimer.start();
    }
}
